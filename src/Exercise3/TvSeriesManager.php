<?php

declare(strict_types=1);

namespace ExadsTest\Exercise3;

class TVSeriesManager {

    private const TV_SHOW_FOUND_PATTERN = "Next airing of '%s' is on next %s at %s";

    private const TV_SHOW_NOT_FOUND_TEXT = "No upcoming airings found.";

    public function __construct(private TvSeriesRepository $tvSeriesRepository) 
    {
    }

    public function showNextAirTime(string $dateTime = null, string $title = null): string
    {
        if ($dateTime === null) {
            $dateTime = date('Y-m-d H:i:s');
        }

        $tvShow = $this->tvSeriesRepository->getNextAirTime($dateTime, $title);

        return empty($tvShow)
            ? self::TV_SHOW_NOT_FOUND_TEXT
            : sprintf(
                self::TV_SHOW_FOUND_PATTERN, 
                $tvShow['title'], 
                ucfirst(strtolower(Weekday::from($tvShow['week_day'])->name)), 
                $tvShow['show_time']
            );
    }
}
