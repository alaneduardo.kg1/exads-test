<?php

declare(strict_types=1);

namespace ExadsTest\Exercise4\Domain;

use ExadsTest\Exercise4\Domain\Entity\Design;

interface PromotionDataProvider
{
    /**
     * @return Design[]|null
     */
    public function getAllDesignsById(int $id): ?array;
}