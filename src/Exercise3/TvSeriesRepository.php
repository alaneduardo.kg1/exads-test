<?php

declare(strict_types=1);

namespace ExadsTest\Exercise3;

use PDO;

class TvSeriesRepository {

    public function __construct(private PDO $pdo) 
    {
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @return array<int, mixed>
     */
    public function getNextAirTime(string $dateTime, string $title = null): ?array
    {
        $sql = "SELECT 
                    tv_series.title, 
                    tv_series_intervals.week_day, 
                    tv_series_intervals.show_time,
                    CONCAT(
                        current_date() + interval (7 + (tv_series_intervals.week_day - 1) - weekday(current_date)) % 7 day, 
                        ' ', 
                        tv_series_intervals.show_time
                    ) AS `datetime`
                FROM tv_series
                INNER JOIN tv_series_intervals ON tv_series.id = tv_series_intervals.id_tv_series
                WHERE CONCAT(
                        current_date + interval (7 + (tv_series_intervals.week_day - 1) - weekday(current_date)) % 7 day, 
                        ' ', 
                        tv_series_intervals.show_time
                    ) >= :dateTime";

        $params = array(':dateTime' => $dateTime);

        if ($title !== null) {
            $sql .= " AND tv_series.title = :title";
            $params[':title'] = $title;
        }

        $sql .= " ORDER BY 4 ASC LIMIT 1";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);

        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($data === false) {
            return null;
        }

        return $data;
    }
}
