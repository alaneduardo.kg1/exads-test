<?php
use ExadsTest\Exercise3\TvSeriesRepository;

require_once(dirname(__DIR__).'/vendor/autoload.php');

use ExadsTest\Exercise3\TVSeriesManager;

$host = getenv("DB_HOST");
$port = getenv("DB_PORT");
$user = getenv("DB_USER");
$dbName = getenv("DB_NAME");

$dbConn = new PDO("mysql:host=$host;dbname=$dbName;port=$port", $user);

$tvSeriesManager = new TVSeriesManager(
    new TvSeriesRepository($dbConn)
);

echo "Next airing without filtering by title and date: " . $tvSeriesManager->showNextAirTime() . "\n\n";
echo "Next airing filtering by Flight Attendant since Feb 28 3pm: " . $tvSeriesManager->showNextAirTime('2024-02-28 15:00:00', 'Flight Attendant') . "\n\n";
echo "Next airing filtering just by Por do Sol: " . $tvSeriesManager->showNextAirTime(null, 'Por do Sol') . "\n\n";
echo "Next airing filtering just by date, since Feb 28 3pm: " . $tvSeriesManager->showNextAirTime('2024-02-28 15:00:00', null) . "\n\n";