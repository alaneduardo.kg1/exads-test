<?php

declare(strict_types=1);

namespace ExadsTest\Exercise4\Infrastructure;

use ExadsTest\Exercise4\Domain\Entity\Design;
use ExadsTest\Exercise4\Domain\PromotionDataProvider;

class AnotherPromotionRepository implements PromotionDataProvider
{
    /**
     * @param array<int, array>
     */
    private array $data = [
        1 => [
            "name" => "Promo 1", 
            "designs" => [
                [ "designId" => 1, "designName" => "Design A", "splitPercent" => 25 ], 
                [ "designId" => 2, "designName" => "Design B", "splitPercent" => 25 ], 
                [ "designId" => 3, "designName" => "Design C", "splitPercent" => 50 ],
            ] 
        ],
        2 => [
            "name" => "Promo 2", 
            "designs" => [
                [ "designId" => 1, "designName" => "Design W", "splitPercent" => 10 ], 
                [ "designId" => 2, "designName" => "Design X", "splitPercent" => 80 ], 
                [ "designId" => 3, "designName" => "Design Z", "splitPercent" => 10 ],
            ] 
        ]
    ];

    /**
     * @return Design[]|null
     */
    public function getAllDesignsById(int $id): ?array
    {
        if (!isset($this->data[$id])) {
            throw new \Exception("Promotion was not found");
        }

        $designs = $this->data[$id]['designs'];

        $this->applyDescendingOrder($designs);

        return array_map(function ($item) {
            return new Design($item['designName'], $item['splitPercent']);
        }, $designs);
    }

    private function applyDescendingOrder(array &$designs): void
    {
        usort($designs, function($a, $b) {
            return $b['splitPercent'] - $a['splitPercent'];
        });
    }
}