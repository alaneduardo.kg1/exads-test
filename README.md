# EXADS Test



## Getting started

Hello. Welcome to the EXADS canditate test. Please find below important instructions about the system requirements, exercises, test structure and how to run it. 

Enjoy :D!

## Exercises

### 1. Prime Numbers
Write a PHP script that prints all integer values from 1 to 100.
Beside each number, print the numbers it is a multiple of (inside brackets and comma-separated). If only multiple of itself then print "[PRIME]".

### 2. ASCII Array
Write a PHP script to generate a random array containing all the ASCII characters from comma (",") to pipe ("|"). Then randomly remove and discard an arbitrary element from this newly generated array.
Write the code to efficiently determine the missing character.

### 3. TV Series
Populate a MySQL (InnoDB) database with data from at least 3 TV Series using the following structure:
```
tv_series -> (id, title, channel, gender);
tv_series_intervals -> (id_tv_series, week_day, show_time);
```

Using OOP, write a code that tells when the next TV Series will air based on the current time-date or an inputted time-date, and that can be optionally filtered by TV Series title.

(Provide the SQL scripts that create and populate the DB)

### 4. A/B Testing
Exads would like to A/B test some promotional designs to see which provides the best conversion rate. Write a snippet of PHP code that redirects end users to the different designs based on the data provided by this library: [Packagist exads/ab-test-data](https://packagist.org/exads/ab-test-data)

The data will be structured as follows:
```
"promotion" => [ 
    "id" => 1,
    "name" => "main", 
    "designs" => [
        [ "designId" => 1, "designName" => "Design 1", "splitPercent" => 50 ], 
        [ "designId" => 2, "designName" => "Design 2", "splitPercent" => 25 ], 
        [ "designId" => 3, "designName" => "Design 3", "splitPercent" => 25 ],
    ] 
]
```
The code needs to be object-oriented and scalable. The number of designs per promotion may vary.

## Test Structure

This test has the following folder structure:

- **/src:** Folder with the class source code of the tests. Each Exercise has its own namespace, e.g. ExadsTest\Exercise1, ExadsTest\Exercise2.
- **/run:** Folder with the bootstrap run of the exercises. Each exercise has its own file, e.g. exercise-1.php, exercise-2.php.

## System Requirements

In order to run the tests contained in this repository, it is needed to have installed in your machine:

- [Composer 2+](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos)
- [PHP 8.0+](https://www.php.net/manual/en/install.php)
- [Docker](https://docs.docker.com/engine/install/)

## Usage

First of all, run the following command to set up the environment.
```
$ composer install
```

To run the exercise, you need to use composer scripts within the correct test-id

```
$ composer run <test-id>
```

When the test ids are:

- **exercise-1** for the Exercise no.1
- **exercise-2** for the Exercise no.2
- **exercise-3** for the Exercise no.3
- **exercise-4** for the Exercise no.4

Example for Exercise no.4:
```
$ composer run exercise-4
```
