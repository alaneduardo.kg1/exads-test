-- Create the database
CREATE DATABASE exads_test;

-- Switch to the created database
USE exads_test;

-- Create table for TV series
CREATE TABLE tv_series (
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255),
    channel VARCHAR(255),
    genre VARCHAR(255)
);

-- Create table for TV series intervals
CREATE TABLE tv_series_intervals (
    id_tv_series INT,
    week_day INT, -- when 0 is Sunday and 6 is Saturday
    show_time TIME,
    FOREIGN KEY (id_tv_series) REFERENCES tv_series(id)
);

-- Populate TV series data
INSERT INTO tv_series (title, channel, genre) VALUES
('Por do Sol', 'RTP 1', 'Comedy'),
('Supernatural', 'Prime Video', 'Sci-Fi'),
('Flight Attendant', 'HBO Max', 'Drama');

-- Populate TV series intervals data
INSERT INTO tv_series_intervals (id_tv_series, week_day, show_time) VALUES
(1, 1, '23:00:00'),
(2, 4, '18:00:00'),
(3, 5, '20:00:00');
