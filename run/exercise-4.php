<?php

require_once(dirname(__DIR__).'/vendor/autoload.php');

use ExadsTest\Exercise4\Domain\ABTester;
use ExadsTest\Exercise4\Infrastructure\ExadsPromotionRepository;
use ExadsTest\Exercise4\Infrastructure\AnotherPromotionRepository;

$testerExads = new ABTester(
    new ExadsPromotionRepository()
);

$promotionId = 1;
$selectedDesignFromExads = $testerExads->getDesign($promotionId);

echo('Location (Source Exads/ABTestData): https://exads.com/designs/' . $selectedDesignFromExads . "\n\n");

$testerArray = new ABTester(
    new AnotherPromotionRepository()
);

$promotionId = 2;
$selectedDesignFromArray = $testerArray->getDesign($promotionId);

echo('Location (Source Array): https://exads.com/designs/' . $selectedDesignFromArray . "\n\n");