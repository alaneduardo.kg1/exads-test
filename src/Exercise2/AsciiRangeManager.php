<?php

namespace ExadsTest\Exercise2;

class AsciiRangeManager 
{
    private array $range = [];

    private string $discardedElement;

    public function __construct(private string $rangeStartsAt, private string $rangeEndsAt)
    {  
        $this->range = range($this->rangeStartsAt, $this->rangeEndsAt);
    }

    public function discardRandomChar(): void 
    {
        $randomChar = array_rand($this->range);

        $this->discardedElement = $this->range[$randomChar];

        unset($this->range[$randomChar]);
    }

    public function getDiscardedElement(): string
    {
        return $this->discardedElement;
    }

    public function findMissingCharacter(): string
    {
        $fullRange = range($this->rangeStartsAt, $this->rangeEndsAt);

        return current(array_diff($fullRange, $this->range));
    }
}
