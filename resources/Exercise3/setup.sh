#!/bin/sh

docker run -d --name=db -e MYSQL_ALLOW_EMPTY_PASSWORD=true -p 3306:3306 mysql
docker cp ./resources/Exercise3/db.sql db:/docker-entrypoint-initdb.d