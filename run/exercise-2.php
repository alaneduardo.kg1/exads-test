<?php

require_once(dirname(__DIR__).'/vendor/autoload.php');

use ExadsTest\Exercise2\AsciiRangeManager;

$manager = new AsciiRangeManager(",", "|");

$manager->discardRandomChar();

echo "Discarded element: " . $manager->getDiscardedElement() . "\n";
echo "Found element: " . $manager->findMissingCharacter() . "\n";