<?php

declare(strict_types=1);

namespace ExadsTest\Exercise4\Domain\Entity;

class Design
{
    public function __construct(private readonly string $name, private readonly int $splitPercent)
    {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSplitPercent(): int
    {
        return $this->splitPercent;
    }
}