<?php

declare(strict_types=1);

namespace ExadsTest\Exercise4\Infrastructure;

use Exads\ABTestData;
use ExadsTest\Exercise4\Domain\Entity\Design;
use ExadsTest\Exercise4\Domain\PromotionDataProvider;

class ExadsPromotionRepository implements PromotionDataProvider
{
    /**
     * The class ABTestData is behaving as Entity and Repository. It would be
     * interesting decouple those 2 responsibilities, using the class as a 
     * dependency like in the code below.
     * 
     * public function __construct(private readonly ABTestData $abTestData)
     * {
     * }
     */

    /**
     * @return Design[]|null
     */
    public function getAllDesignsById(int $id): ?array
    {
        $abTest = new ABTestData($id);

        $designs = $abTest->getAllDesigns();

        $this->applyDescendingOrder($designs);

        return array_map(function ($item) {
            return new Design($item['designName'], $item['splitPercent']);
        }, $designs);
    }

    private function applyDescendingOrder(array &$designs): void
    {
        usort($designs, function($a, $b) {
            return $b['splitPercent'] - $a['splitPercent'];
        });
    }
}