<?php

declare(strict_types=1);

namespace ExadsTest\Exercise4\Domain;

class ABTester 
{
    public function __construct(private readonly PromotionDataProvider $promotionDataProvider)
    {
    }

    public function getDesign(int $promotionId): string 
    {
        $designList = $this->promotionDataProvider->getAllDesignsById($promotionId);

        $index = mt_rand(1, 100);
        $cumulativePerc = 0;
        
        foreach ($designList as $design) {
            $cumulativePerc += $design->getSplitPercent();

            if ($index <= $cumulativePerc) {
                return $design->getName();
            }
        }

        return $designList[0]->getName();
    }
}