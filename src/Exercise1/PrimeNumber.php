<?php

declare(strict_types=1);

namespace ExadsTest\Exercise1;

class PrimeNumber {

    private const LINE_PATTERN = "%d [%s]\n";

    private const PRIME_TEXT = 'PRIME';

    private const NUMBER_SEPARATOR = ", ";

    public function __construct(private int $rangeStartsAt, private int $rangeEndsAt)
    {
    }

    /**
     * @return int[]
     */
    private function multiplesOf($number): array 
    {
        $multiples = [];

        for ($i = 1; $i <= $number; $i++) {
            if ($number % $i == 0) {
                $multiples[] = $i;
            }
        }

        return $multiples;
    }

    public function show(): string
    {
        $returnText = '';

        for ($i = $this->rangeStartsAt; $i <= $this->rangeEndsAt; $i++) {
            $multiples = $this->multiplesOf($i);

            $bracketText = (count($multiples) == 2) ? self::PRIME_TEXT : implode(self::NUMBER_SEPARATOR, $multiples);

            $returnText .= sprintf(self::LINE_PATTERN, $i, $bracketText);
        }

        return $returnText;
    }
}